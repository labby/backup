<?php

/**
 *	@module			Backup
 *	@version		see info.php of this module
 *	@author			cms-lab
 *	@copyright		2020-2024 cms-lab
 *	@license		GNU General Public License
 *	@license_terms	please see info.php of this module 
 *	@platform		see info.php of this module
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

$MOD_BACKUP = array(
	'all'		=> "Database and Files",
	'backup'	=> "start backup",	
	'chose'		=> "Please choose backup task ",
	'db_only'	=> "Database only",
	'info'		=> "Backup data and files may need some time, depending on number and size of files and size of database!<br /> Maximum execution time was set to 300 for this job!",
	'save_db_ok'	=> "The database backup into /temp folder was successfully done!",
	'save_files_ok'	=> "The files backup into /temp folder was successfully done!",

);
