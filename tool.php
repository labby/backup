<?php

/**
 *	@module			Backup
 *	@version		see info.php of this module
 *	@author			cms-lab
 *	@copyright		2020-2024 cms-lab
 *	@license		GNU General Public License
 *	@license_terms	please see info.php of this module 
 *	@platform		see info.php of this module
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php


// get instance of functions file
$oBU = backup::getInstance();

if ( isset( $_GET['tool'] ))
{
	$oBU->display_form();
} 
else 
{
	die('something went wrong');
}

if ( isset( $_POST['start'] ))
{
	$oBU->start_backup(intval($_POST['check']));
} 
