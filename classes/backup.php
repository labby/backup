<?php

/**
 *	@module			Backup
 *	@version		see info.php of this module
 *	@author			cms-lab
 *	@copyright		2020-2024 cms-lab
 *	@license		GNU General Public License
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 */

class backup extends LEPTON_abstract 
{
	public int $timestamp = 0;
	public string $action_url = '';
	public string $backup_path = '';
	public string $filename_db = '0';
	public string $filename_files = '0';

	public object|null $oTwig = null;
	public LEPTON_database $database;
	static $instance;

	public function initialize()
	{
		$this->database = LEPTON_database::getInstance();
		$this->oTwig = lib_twig_box::getInstance();
		$this->oTwig->registerModule('backup');
		$this->action_url = ADMIN_URL."/admintools/tool.php?tool=backup";		
		$this->init_tool();
	}

	public function init_tool( $sToolname = '' )
	{
		$this->timestamp = time();
		$this->filename_db = date('Y-m-d_H-i-s',time()).'.sql';
		$this->filename_files = date('Y-m-d_H-i-s',time()).'.zip';
		$this->backup_path = lib_lepton::getToolInstance('mysqldump')::$defaultDir;
	}

	public function display_form()
	{

		// data for twig template engine
		$templateValues = array(
			'oBU'		=> $this,
			'readme_link'	=> "https://cms-lab.com/_documentation/backup/readme.php",
			'leptoken'		=> get_leptoken()

		);

		//	get the template-engine
		echo $this->oTwig->render(
			"@backup/tool.lte",	//	template-filename
			$templateValues		//	template-data
		);

	}
	
	public function start_backup($id = -1)
	{
		if($id == intval($_POST['check']))
		{
			if($_POST['modus'] == 0 || $_POST['modus'] == 1)		// backup db
			{
				$sParam2 = array(
					"compress" => 'none'
				);

				$dump = lib_lepton::getToolInstance("mysqldump", $sParam2);
				$dump->start($this->backup_path.$this->filename_db);
				
				echo LEPTON_tools::display($this->language['save_db_ok'],'pre','ui green message');	
			}
			
			if($_POST['modus'] == 1)		// backup files also
			{
				ini_set('max_execution_time',300);
				$oZIP = lib_lepton::getToolInstance("php-zip");
				$oZIP->addDirRecursive(LEPTON_PATH);
				$oZIP->saveAsFile($this->backup_path.$this->filename_files); // save the archive to a file
				$oZIP->close();	
				
				echo LEPTON_tools::display($this->language['save_files_ok'],'pre','ui green message');				
			}		
		}
	
		header("Refresh:3; url=".$this->action_url."&leptoken=".get_leptoken());	
	}	

	/** =========================================================================
	 *
	 * Show an info popup
	 *
	 * @access  public
	 * @param   $modvalues  As optional array containing module specialized values
	 * @param   $bPrompt    True for direct output via echo, false for returning the generated source.
	 * @return  mixed       Depending on the $bPrompt param: boolean or string.
	 */
	public function showmodinfo( $modvalues = null, $bPrompt = true )
	{
		// prepare array with module specific values
		$modvalues = array(
			"BUTTONS"		=> array(
			"README"		=> array( 	"AVAILABLE"	=> true,
										"URL"		=> "https://cms-lab.com/_documentation/backup/readme.php"
				)
			)
		);

		// show module info
		return parent::showmodinfo( $modvalues );
	}
}

